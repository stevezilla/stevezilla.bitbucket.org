/*
 * jQuery UI Font Selector Widget
 *
 * Copyright 2012, Olav Andreas Lindekleiv (http://lindekleiv.com/)
 * Available under the BSD License
 * See the LICENSE file or http://opensource.org/licenses/BSD-3-Clause
 *
 * Changed by Steve Scheidecker (http://stevezilla.com) to allow multiple font selectors on a single page.
 * Available on BitBucket at
 * https://bitbucket.org/sscheidecker/jquery-ui-fontselector-multiples
 */

$.widget('oal.ddFancy', {
  options: {
    inSpeed: 500,
    outSpeed: 250,
    closeOnSelect: true,
    iconWidth: 32,
    iconHeight: 32,
    height:25
  },
  _create: function() {
	this.height = this.options.height;
    var items, item, itemEl, itemLabel, itemImage, itemStyle, itemName, label, _i, _j, _len, _len2, _ref, _ref2,
      _this = this;
    this.element.hide();
    _ref = this.element.children();
	items = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      item = _ref[_i];
      itemLabel = $(item).text();
      itemName = $(item).attr('value');
      itemImage = $(item).attr('data-image');
	  itemStyle = $(item).attr('data-style');
	  if(!itemStyle) itemStyle = '';
      if ($(item).attr('selected')) this.selected = itemName;
      items.push([itemName, itemLabel, itemImage, itemStyle]);
      if(itemImage) this.hasImages = true;
    }
    if (!this.selected) this.selected = items[0][0];

	if(this.element.css('width')){
		this.width = parseInt(this.element.css('width'));
	} else {
		this.width = 125;
	}

	if(this.hasImages){
		if(this.height < (this.options.iconHeight+6)) this.height = parseInt(this.options.iconHeight) + 6;
	}
    this.dropdown = $('<div class="ddFancy ui-widget" style="width:' + this.width + 'px;height:' + this.height + 'px"><span class="handle pointDown"></span></div>');
	
    this.list = $('<ul class="items" style="z-index:2;width:' + this.width + 'px;top:' + this.height + 'px;"></ul>');
    for (_j = 0, _len2 = items.length; _j < _len2; _j++) {
      item = items[_j];
      _ref2 = item, item = _ref2[0], label = _ref2[1], itemImage = _ref2[2], itemStyle = _ref2[3];
	  if(itemImage){
		itemEl = $('<li style="width:' + this.width + 'px;line-height:' + this.height + 'px;height:'+ this.height + 'px;"><img style="width:' + this.options.iconWidth + 'px;height:' + this.options.iconHeight + 'px;" src="' + itemImage + '" /><span style="' + itemStyle + '">' + label + "</span></li>");
 	  } else {
      	itemEl = $('<li style="width:' + this.width + 'px;line-height:' + this.height + 'px;height:'+ this.height + 'px;"><span style="' + itemStyle + '">' + label + "</span></li>");
      }
      itemEl.data('value', item);
	  itemEl.data('image', itemImage);
	  itemEl.data('dstyle', itemStyle);
      if (item === this.selected) {
        itemEl.addClass('selected');
		if(itemImage){
        	this.dropdown.prepend($('<h4 class="selected handle" style="width:' + (this.width-35)+'px;line-height:' + (this.height) + 'px;height:' + this.height + 'px;"><img style="width:' + this.options.iconWidth + 'px;height:' + this.options.iconHeight + 'px;" src="' + itemImage + '" /><span style="' + itemStyle + '">' + label + '</span></h4>'));
      	} else {
			this.dropdown.prepend($('<h4 class="selected handle" style="width:' + (this.width-35)+'px;line-height:' + (this.height) + 'px;height:' + this.height + 'px;"><span style="' + itemStyle + '">' + label + '</span></h4>'));
        }
	  }
      this.list.append(itemEl);
    }
    this.dropdown.append(this.list);
    this.element.after(this.dropdown);
	
	return $(this.dropdown).find(".handle").click(function() {
      return _this._toggleOpen();
    });


  },
  _toggleOpen: function() {
    var _this = this;
    if (this.list.is(':visible')) {
	  this.dropdown.find('span.handle').removeClass("pointUp").addClass("pointDown");
      return this.list.slideUp(this.options.outSpeed);
    } else {
      this.dropdown.find('span.handle').removeClass("pointDown").addClass("pointUp");
      this.list.slideDown(this.options.inSpeed);
	  return $(this.dropdown).find('ul.items li').click(function(e) {
		if(_this.options.closeOnSelect){
			_this.dropdown.find('span.handle').removeClass("pointUp").addClass("pointDown");
			_this.list.slideUp(_this.options.outSpeed);
		}
		
        var item, itemLi, itemName, itemOption, itemImage, itemStyle, label, oldItem, _i, _j, _len, _len2, _ref, _ref2, _results;

		if($(e.target).is("li")){
        	item = $(e.target).data('value');
	        itemImage = $(e.target).data('image');
			itemStyle = $(e.target).data('dstyle');
	        label = $(e.target).text();
		} else {
			item = $(e.target).parent().data('value');
	        itemImage = $(e.target).parent().data('image');
			itemStyle = $(e.target).parent().data('dstyle');
	        label = $(e.target).parent().text();
		}
        oldItem = _this.selected;
        if (item === oldItem) return false;
        _this._trigger('itemChange', null, {
          item: item,
          oldItem: oldItem
        });
        _this.selected = item;
        
		$(_this.element).find("option").attr("selected",false);
		$(_this.element).find('option[value="' + item + '"]').attr("selected","true");

		if(itemImage){
			$(_this.dropdown).find('h4.selected').html('<img style="width:' + _this.options.iconWidth + 'px;height:' + _this.options.iconHeight + 'px;" src="' + itemImage + '" /><span style="' + itemStyle + '">'+label+'</span>');
		} else {
			$(_this.dropdown).find('h4.selected').html('<span style="' + itemStyle + '">'+label+'</span>');
		}

        _ref = _this.list.children();
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          itemLi = _ref[_i];
          if ($(itemLi).data('value') === item) {
            $(itemLi).addClass('selected');
          } else if ($(itemLi).data('value') === oldItem) {
            $(itemLi).removeClass('selected');
          }
        }
        _ref2 = _this.element.children();
        _results = [];
        for (_j = 0, _len2 = _ref2.length; _j < _len2; _j++) {
          itemOption = _ref2[_j];
          itemName = $(itemOption).val();
          if (itemName === item) {
            _results.push($(itemOption).attr('selected', 'selected'));
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      });
    }
  },
  destroy: function() {
    return Widget.prototype.destroy.call(this);
  }
});