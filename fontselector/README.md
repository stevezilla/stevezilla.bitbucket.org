jQuery UI Multiple Font selector widget
==============================

This is a font selector widget that allows the selection of multiple fonts on a single page using the Google Web Fonts library. The original version was made by [Olav Andreas Lindekleiv][1], and modified to its current state by [Steve Scheidecker][4]. It is available on [BitBucket][2]. You can also try out a live demo [here][3]</a>.</p>



Usage
-----

To use the widget, simply create a select element with the fonts you want to use (make sure they're available from the Google Web Fonts library first!), and enable it.

      <select id="fonts">
        <option value="Chelsea Market">Chelsea Market</option>
        <option value="Droid Serif" selected="selected">Droid Serif</option>
        <option value="Ruluko">Ruluko</option>
        <option value="Ruda">Ruda</option>
        <option value="Magra">Magra</option>
        <option value="Esteban">Esteban</option>
        <option value="Lora">Lora</option>
        <option value="Jura">Jura</option>
      </select>


To enable it, do:

      $('select#fonts').fontSelector({
        options: {
          inSpeed: 250,
          outSpeed: "slow",
        },
        fontChange: function(e, ui) {
          alert("The font is set to "+ui.font+" (was "+ui.oldFont+" before)");
        },
        styleChange: function(e, ui) {
          alert("The value of "+ui.style+" was set to "+ui.value);
        },
        closeOnSelect: true, // Closes the select box when a selection is made
        styleToggles: ['Bold','Italic','Underline','Strike','Blink'] // Define which styles to allow user to select.
      });

The options are optional. Default values are 500 ms for inSpeed and 250 ms for outSpeed.


License
-------

The jQuery UI Font selector widget is available under the BSD License. Please read it (see the LICENSE file). It's only a few lines.


  [1]: http://lindekleiv.com/
  [2]: https://bitbucket.org/sscheidecker/jquery-ui-fontselector-multiples
  [3]: http://sscheidecker.bitbucket.org/jquery-ui-fontselector-multiples/
  [4]: http://stevezilla.com